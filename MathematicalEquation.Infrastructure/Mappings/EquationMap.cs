﻿using MathematicalEquation.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace MathematicalEquation.Infrastructure.Mappings
{
    public class EquationMap : IEntityTypeConfiguration<Equation>
    {
        public void Configure(EntityTypeBuilder<Equation> builder)
        {
            builder.Property(c => c.Id).HasColumnName("Id");

            builder.Property(c => c.Xvalue).HasColumnName("Xvalue").IsRequired();

            builder.Property(c => c.Yvalue).HasColumnName("Yvalue").IsRequired();

            builder.Property(c => c.Expression).HasColumnName("Expression")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}
