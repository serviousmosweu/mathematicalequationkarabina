﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MathematicalEquation.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Equations",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Xvalue = table.Column<int>(type: "int", nullable: false),
                    Yvalue = table.Column<int>(type: "int", nullable: false),
                    Expression = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    CalculatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equations", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Equations");
        }
    }
}
