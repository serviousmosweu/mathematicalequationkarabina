﻿using MathematicalEquation.Domain.Base;
using MathematicalEquation.Domain.Interfaces;
using MathematicalEquation.Domain.Models;
using MathematicalEquation.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MathematicalEquation.Infrastructure.Repository
{
    public class EquationRepository : IEquationRepository
    {

        protected readonly EquationContext _equationContext;
        protected readonly DbSet<Equation> DbSet;
        public EquationRepository(EquationContext equationContext)
        {
            _equationContext = equationContext;
            DbSet = _equationContext.Set<Equation>();
        }

        
        public IUnitOfWork UnitOfWork => _equationContext;

        public async Task<int> Add(Equation equation)
        {
            //DbSet.Add(equation);
            _equationContext.Equations.Add(equation);
            var result = await _equationContext.SaveChangesAsync();

            return result;
        }

        public void Dispose()
        {
            _equationContext.Dispose(); 
        }

        public async Task<List<Equation>> GetAll()
        {
            return await DbSet.ToListAsync();
        }

        public async Task<Equation> GetById(Guid id)
        {
            //throw new NotImplementedException();
            return await DbSet.FindAsync(id);
        }

        public void Remove(Equation equation)
        {
            DbSet.Remove(equation);
        }

        public async Task<int> Update(Equation equation)
        {
            //DbSet.Update(equation);
            _equationContext.Equations.Update(equation);
            var result = await _equationContext.SaveChangesAsync();

            return result;
        }
    }
}
