﻿
using MathematicalEquation.Domain.Base;
using System;

namespace MathematicalEquation.Domain.Models
{
    public class Equation : Entity, IAggregateRoot
    {
        protected Equation() { }

        public Equation(long id,int xvalue, int yvalue, string expression, DateTime calculatedDate)
        {
            Id = id;
            Xvalue = xvalue;
            Yvalue = yvalue;
            Expression = expression;
            CalculatedDate = calculatedDate;
        }
        public int Xvalue { get; private set; }
        public int Yvalue { get; private set; }
        public string Expression { get; private set; }
        public DateTime CalculatedDate { get; private set; }
    }
}
