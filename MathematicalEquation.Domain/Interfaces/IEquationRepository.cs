﻿using MathematicalEquation.Domain.Base;
using MathematicalEquation.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MathematicalEquation.Domain.Interfaces
{
    public interface IEquationRepository : IRepository<Equation>
    {
        Task<Equation> GetById(Guid id);
        Task<List<Equation>> GetAll();

        Task<int> Add(Equation equation);
        Task<int> Update(Equation equation);
        void Remove(Equation equation);
    }
}
