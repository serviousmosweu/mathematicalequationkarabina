﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathematicalEquation.WebUI.Models
{
    public class Calculator
    {
        public Calculator()
        {
        }

        public int Xvalue { get; set; }
        public int CalculatedValue { get; set; }
        public string Expression { get; set; }

        private string[] _operators = { "-", "+", "/", "*", "^","\\sqrt" };

        private Func<double, double, double>[] _operations = 
        {
            (a1, a2) => a1 - a2,
            (a1, a2) => a1 + a2,
            (a1, a2) => a1 / a2,
            (a1, a2) => a1 * a2,
            (a1, a2) => Math.Pow(a1, a2),
            (a1,a2) => Math.Sqrt(a2)
        };

        public int Calculate(int xvalue, string expression)
        {
            expression = expression.Replace("x", "*x");
            List<string> tokens = getTokens(expression);
            Stack<double> operandStack = new Stack<double>();
            Stack<string> operatorStack = new Stack<string>();
            int tokenIndex = 0;
            

            while (tokenIndex < tokens.Count)
            {
                string token = tokens[tokenIndex];
                if (token == "(")
                {
                    string subExpr = getSubExpression(tokens, ref tokenIndex);
                    operandStack.Push(Calculate(xvalue, subExpr));
                    continue;
                }
                if (token == ")")
                {
                    throw new ArgumentException("Mis-matched parentheses in expression");
                }
                //If this is an operator  
                if (Array.IndexOf(_operators, token) >= 0)
                {
                    while (operatorStack.Count > 0 && Array.IndexOf(_operators, token) < Array.IndexOf(_operators, operatorStack.Peek()))
                    {
                        string op = operatorStack.Pop();
                        double arg2 = operandStack.Pop();
                        try
                        {
                            double arg1 = operandStack.Pop();
                            operandStack.Push(_operations[Array.IndexOf(_operators, op)](arg1, arg2));
                        }
                        catch (Exception ex)
                        {
                            operandStack.Push(_operations[Array.IndexOf(_operators, op)](1, arg2));
                        }
                    }
                    operatorStack.Push(token);
                }
                else
                {
                    //Get the value of X
                    token = token.Replace("x", xvalue.ToString());
                    try
                    {
                        operandStack.Push(double.Parse(token));
                    }
                    catch (Exception ex)
                    {
                        //throw new ArgumentException("Mis-matched in expression");
                        string error = ex.Message;
                    }
                }
                tokenIndex += 1;
            }

            while (operatorStack.Count > 0)
            {
                string op = operatorStack.Pop();
                double arg2 = operandStack.Pop();

                try
                {
                    double arg1 = operandStack.Pop();
                    operandStack.Push(_operations[Array.IndexOf(_operators, op)](arg1, arg2));
                }
                catch (Exception ex)
                {
                    operandStack.Push(_operations[Array.IndexOf(_operators, op)](1, arg2));
                }
            }

            try
            {
                return Convert.ToInt32(operandStack.Pop());
            }catch(Exception ex)
            {
                return 0;
            }
        }

        private List<string> getTokens(string expression)
        {
            string operators = "()^*/+-\\";
            //string[] 
            List<string> tokens = new List<string>();
            StringBuilder sb = new StringBuilder();
            char[] expressionArray = expression.ToCharArray();
            if (expressionArray[0] == '*')
            {
                expression = expression.Remove(0, 1);

                expressionArray = expression.ToCharArray();
            }
            foreach (char c in expressionArray)
            {
                if(c == 'y' || c == '=')
                {

                }
                else if (operators.IndexOf(c) >= 0)
                {
                    if ((sb.Length > 0))
                    {
                        tokens.Add(sb.ToString());
                        sb.Length = 0;
                    }
                    if (c == '\\')
                    {
                        tokens.Add("\\sqrt");
                        return tokens;
                    }
                    else
                    {
                        tokens.Add(c.ToString());
                    }
                }
                else
                {
                    sb.Append(c);
                }
            }
            //Remove Y= from expression

            //foreach (char c in expression.Replace("", string.Empty))
            //{
            //    if (operators.IndexOf(c) >= 0)
            //    {
            //        if ((sb.Length > 0))
            //        {
            //            tokens.Add(sb.ToString());
            //            sb.Length = 0;
            //        }
            //        tokens.Add(c.ToString());
            //    }
            //    else
            //    {
            //        sb.Append(c);
            //    }
            //}

            if ((sb.Length > 0))
            {
                tokens.Add(sb.ToString());
            }
            return tokens;
        }

        private string getSubExpression(List<string> tokens, ref int index)
        {
            StringBuilder subExpr = new StringBuilder();
            int parenlevels = 1;
            index += 1;
            while (index < tokens.Count && parenlevels > 0)
            {
                string token = tokens[index];
                if (tokens[index] == "(")
                {
                    parenlevels += 1;
                }

                if (tokens[index] == ")")
                {
                    parenlevels -= 1;
                }

                if (parenlevels > 0)
                {
                    subExpr.Append(token);
                }

                index += 1;
            }

            if ((parenlevels > 0))
            {
                throw new ArgumentException("Mis-matched parentheses in expression");
            }
            return subExpr.ToString();
        }

    }
}
