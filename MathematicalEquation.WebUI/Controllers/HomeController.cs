﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MathematicalEquation.WebUI.Models;
using MathematicalEquation.Domain.Interfaces;
using AutoMapper;
using MathematicalEquation.Domain.Models;
using MathematicalEquation.WebUI.ViewModel;
using System.Net;

namespace MathematicalEquation.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IEquationRepository _equationRepository;
        private readonly IMapper _mapper;

        public HomeController(ILogger<HomeController> logger, IEquationRepository equationRepository, IMapper mapper)
        {
            _logger = logger;
            _equationRepository = equationRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult<EquationDTO>> Index()
        {
            List<Equation> equations;
            equations = await _equationRepository.GetAll();

            var equationDTO = _mapper.Map<List<Equation>, List<EquationDTO>>(equations);

            return View(equationDTO);
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(EquationDTO equationDTO)
        {

            if (equationDTO.Expression != null && equationDTO.Xvalue > 0)
            {
                Calculator calculator = new Calculator();
                var yvalue = calculator.Calculate(equationDTO.Xvalue, equationDTO.Expression);

                EquationDTO calculated = new EquationDTO();
                calculated.Xvalue = equationDTO.Xvalue;
                calculated.Yvalue = yvalue;
                calculated.Expression = equationDTO.Expression;
                //ViewData["calculatedData"] = calculated;
                return RedirectToAction("Save", calculated);
            }

            return View();
        }

        [HttpGet]
        public IActionResult Save(EquationDTO equation)
        {

            if (equation == null) return View();
            return View(equation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(bool saved,EquationDTO saveEquation)
        {
            EquationDTO saveDTO = new EquationDTO();
            saveDTO.Xvalue = saveEquation.Xvalue;
            saveDTO.Yvalue = saveEquation.Yvalue;
            saveDTO.Expression = saveEquation.Expression;
            saveDTO.CalculatedDate = DateTime.Now;

            Equation equation;

            //saveEquation.CalculatedDate = DateTime.Now;
            equation = _mapper.Map<EquationDTO, Equation>(saveDTO);

            var results = await _equationRepository.Add(equation);
            if(results > 0) {
                return RedirectToAction("Index");
            }
            
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
