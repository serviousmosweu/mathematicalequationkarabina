﻿using AutoMapper;
using MathematicalEquation.Domain.Models;
using MathematicalEquation.WebUI.ViewModel;

namespace MathematicalEquation.WebUI.MapperProfiles
{
    public class EquationProfile : Profile
    {
        public EquationProfile()
        {
            CreateMap<Equation, EquationDTO>().ReverseMap();
        }
    }
}
