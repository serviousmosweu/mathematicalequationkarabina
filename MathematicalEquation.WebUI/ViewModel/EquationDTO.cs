﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MathematicalEquation.WebUI.ViewModel
{
    public class EquationDTO
    {
        /// <summary>
        /// Equation Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        ///  X Value
        /// </summary>
        [Display(Name = "X Value")]
        public int Xvalue { get;  set; }
        /// <summary>
        ///  Y Value
        /// </summary>
        [Display(Name = "Calculated Y Value")]
        public int Yvalue { get;  set; }
        /// <summary>
        /// Mathematical Expression
        /// </summary>
        [Display(Name = "Mathematical Expression")]
        public string Expression { get;  set; }
        /// <summary>
        /// Calculated Date
        /// </summary>
        [Display(Name = "Calculated Date")]
        public DateTime CalculatedDate { get;  set; }
    }
}
